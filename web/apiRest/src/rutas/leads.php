<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;




$app = new \Slim\App;
$app->get('/api/get-leads', function(Request $request, Response $response ){
    $sql = "SELECT ID, AnioFinColegio, ApellidoPaterno, ApellidoMaterno, Nombres, Documento, Email, Celular, url_source, Indicador, Mundo, Carrera, Sede, AceptacionTerminos, DateRegister, AceptaPublicidad  FROM leads_tls ORDER BY ID DESC ";
    try {
        $db = new db();
        $db = $db->conectDB();
        $result = $db->query($sql);
        if($result->rowCount()>0){
            $registros= $result->fetchAll(PDO::FETCH_OBJ);
            echo json_encode($registros, JSON_PRETTY_PRINT );
        }else{
            echo json_encode('no hay registros');
        }

        $result = null;
        $registros = null;

    } catch (PDOException $e) {

        echo '{"error": {"text": }'.$e->getMessage().'}';
      
    }

});


$app->get('/api/get-leads/{id}', function(Request $request, Response $response ){
    $ID = $request->getAttribute('id');
    $sql = "SELECT * FROM leads_tls WHERE ID = $ID";
    try {
        $db = new db();
        $db = $db->conectDB();
        $result = $db->query($sql);
        if($result->rowCount()>0){
            $registros= $result->fetchAll(PDO::FETCH_OBJ);
            echo json_encode($registros);
        }else{
            echo json_encode('no hay registros '. $ID);
        }

        $result = null;
        $registros = null;

    } catch (PDOException $e) {

        echo '{"error": {"text": }'.$e->getMessage().'}';
      
    }
});

$app->get('/api/get-leads/{inicial}/{final}', function(Request $request, Response $response ){
        $inicial = $request->getAttribute('inicial') . ' 00:00:00';
        $final = $request->getAttribute('final') . ' 23:59:59';
        $sql = "SELECT ID, AnioFinColegio, ApellidoPaterno, ApellidoMaterno, Nombres, Documento, Email, Celular, url_source, Indicador, Mundo, Carrera, Sede, AceptacionTerminos, DateRegister FROM leads_tls WHERE DateRegister BETWEEN '$inicial' AND '$final'";
        try {
            $db = new db();
            $db = $db->conectDB();
            $result = $db->query($sql);
            if($result->rowCount()>0){
                $registros= $result->fetchAll(PDO::FETCH_OBJ);
                echo json_encode($registros);
            }else{
                echo json_encode('no hay registros en las fechas indicadas '.$sql );
            }

            $result = null;
            $registros = null;

        } catch (PDOException $e) {

            echo '{"error": {"text": }'.$e->getMessage().'}';


        
        }
        //  echo json_encode('no hay registros en las fechas indicadas '. $inicial .'   '.$final);
});

$app->post('/api/post-leads', function(Request $request, Response $response ){
    $AnioFinColegio = $request->getParam('AnioFinColegio');
    $ApellidoPaterno = $request->getParam('ApellidoPaterno');
    $ApellidoMaterno = $request->getParam('ApellidoMaterno');
    $Carrera = $request->getParam('Carrera');
    $Celular = $request->getParam('Celular');
    $Documento = $request->getParam('Documento');
    $Email = $request->getParam('Email');
    $FormularioURL = $request->getParam('FormularioURL');
    $GradoAcademico = $request->getParam('GradoAcademico');
    $Indicador = $request->getParam('Indicador');
    $MedioContacto = $request->getParam('MedioContacto');
    $Mensaje = $request->getParam('Mensaje');
    $Mundo = $request->getParam('Mundo');
    $Nombres = $request->getParam('Nombres');
    $Sede = $request->getParam('Sede');
    $TipoContacto = $request->getParam('TipoContacto');
    $TipoDocumento = $request->getParam('TipoDocumento');
    $AceptacionTerminos = $request->getParam('AceptacionTerminos');
    $FechaAceptacionTerminos = $request->getParam('FechaAceptacionTerminos');
    $url_source = $request->getParam('url_source');
    $utm_medium = $request->getParam('utm_medium');
    $utm_campaign = $request->getParam('utm_campaign');
    $utm_content = $request->getParam('utm_content');
    $utm_source = $request->getParam('utm_source');
    $utm_term = $request->getParam('utm_term');
    $fbclid = $request->getParam('fbclid');
    $DateRegister = date('Y-m-d H:i:s'); 
    $AceptaPublicidad = $request->getParam('AceptaPublicidad');
    $cookieGA = $request->getParam('cookieGA');
    $leadId =  hash('adler32',$request->getParam('Email'));

    $sql = "INSERT INTO leads_tls
            (
                AnioFinColegio,
                ApellidoPaterno,
                ApellidoMaterno,
                Carrera,
                Celular,
                Documento,
                Email,
                FormularioURL,
                GradoAcademico,
                Indicador,
                MedioContacto,
                Mensaje,
                Mundo,
                Nombres,
                Sede,
                TipoContacto,
                TipoDocumento,
                AceptacionTerminos,
                FechaAceptacionTerminos,
                url_source,
                utm_medium,
                utm_campaign,
                utm_content,
                utm_source,
                utm_term,
                fbclid,
                DateRegister,
                AceptaPublicidad,
                cookieGA,
                leadId
            ) 
            VALUES
            (
                :AnioFinColegio,
                :ApellidoPaterno,
                :ApellidoMaterno,
                :Carrera,
                :Celular,
                :Documento,
                :Email,
                :FormularioURL,
                :GradoAcademico,
                :Indicador,
                :MedioContacto,
                :Mensaje,
                :Mundo,
                :Nombres,
                :Sede,
                :TipoContacto,
                :TipoDocumento,
                :AceptacionTerminos,
                :FechaAceptacionTerminos,
                :url_source,
                :utm_medium,
                :utm_campaign,
                :utm_content,
                :utm_source,
                :utm_term,
                :fbclid,
                :DateRegister,
                :AceptaPublicidad,
                :cookieGA,
                :leadId
            )";

try {
        $db = new db();
        $db = $db->conectDB();
        $result = $db->prepare($sql);
        $result->bindParam(':AnioFinColegio', $AnioFinColegio);
        $result->bindParam(':ApellidoPaterno', $ApellidoPaterno);
        $result->bindParam(':ApellidoMaterno', $ApellidoMaterno);
        $result->bindParam(':Carrera', $Carrera);
        $result->bindParam(':Celular', $Celular);
        $result->bindParam(':Documento', $Documento);
        $result->bindParam(':Email', $Email);
        $result->bindParam(':FormularioURL', $FormularioURL);
        $result->bindParam(':GradoAcademico', $GradoAcademico);
        $result->bindParam(':Indicador', $Indicador);
        $result->bindParam(':MedioContacto', $MedioContacto);
        $result->bindParam(':Mensaje', $Mensaje);
        $result->bindParam(':Mundo', $Mundo);
        $result->bindParam(':Nombres', $Nombres);
        $result->bindParam(':Sede', $Sede);
        $result->bindParam(':TipoContacto', $TipoContacto);
        $result->bindParam(':TipoDocumento', $TipoDocumento);
        $result->bindParam(':AceptacionTerminos', $AceptacionTerminos);
        $result->bindParam(':FechaAceptacionTerminos', $FechaAceptacionTerminos);
        $result->bindParam(':url_source', $url_source);
        $result->bindParam(':utm_medium', $utm_medium);
        $result->bindParam(':utm_campaign', $utm_campaign);
        $result->bindParam(':utm_content', $utm_content);
        $result->bindParam(':utm_source', $utm_source);
        $result->bindParam(':utm_term', $utm_term);
        $result->bindParam(':fbclid', $fbclid);
        $result->bindParam(':DateRegister', $DateRegister);
        $result->bindParam(':AceptaPublicidad', $AceptaPublicidad);
        $result->bindParam(':cookieGA', $cookieGA);
        $result->bindParam(':leadId', $leadId);          
        $result->execute();
        echo json_encode('Nuevo Lead Guardado');
        $result = null;
        $registros = null;

    } catch (PDOException $e) {

        echo '{"error": {"text": }'.$e->getMessage().'}';
      
    }
});

$app->post('/api/post-leads-crm', function(Request $request, Response $response ){
    $AnioFinColegio = $request->getParam('AnioFinColegio');
    $ApellidoPaterno = $request->getParam('ApellidoPaterno');
    $ApellidoMaterno = $request->getParam('ApellidoMaterno');
    $Carrera = $request->getParam('Carrera');
    $Celular = $request->getParam('Celular');
    $Documento = $request->getParam('Documento');
    $Email = $request->getParam('Email');
    $FormularioURL = $request->getParam('FormularioURL');
    $GradoAcademico = $request->getParam('GradoAcademico');
    $Indicador = $request->getParam('Indicador');
    $MedioContacto = $request->getParam('MedioContacto');
    $Mensaje = $request->getParam('Mensaje');
    $Mundo = $request->getParam('Mundo');
    $Nombres = $request->getParam('Nombres');
    $Sede = $request->getParam('Sede');
    $TipoContacto = $request->getParam('TipoContacto');
    $TipoDocumento = $request->getParam('TipoDocumento');
    $AceptacionTerminos = $request->getParam('AceptacionTerminos');
    $FechaAceptacionTerminos = $request->getParam('FechaAceptacionTerminos');
    $url_source = $request->getParam('url_source');
    $utm_medium = $request->getParam('utm_medium');
    $utm_campaign = $request->getParam('utm_campaign');
    $utm_content = $request->getParam('utm_content');
    $utm_source = $request->getParam('utm_source');
    $utm_term = $request->getParam('utm_term');
    $fbclid = $request->getParam('fbclid');
    $DateRegister = date("m/d/Y h:i");
    $AceptaPublicidad = $request->getParam('AceptaPublicidad');
    $cookieGA = $request->getParam('cookieGA');
    $leadId =  hash('adler32',$request->getParam('Email'));


	try {

		$params = array(
			utf8_encode('Anio_Fin_Colegio') => $AnioFinColegio,
			utf8_encode('Apellido_Paterno') => $ApellidoPaterno,
			utf8_encode('Apellido_Materno') => $ApellidoMaterno,
			utf8_encode('Carrera') => $Carrera,
			utf8_encode('Celular') => $Celular,
			utf8_encode('Documento') => $Documento,
			utf8_encode('Email') =>  $Email,
			utf8_encode('Formulario') => $FormularioURL,
			utf8_encode('Grado_Academico') => $GradoAcademico,
			utf8_encode('Indicador') => $Indicador,
			utf8_encode('Mensaje') => $Mensaje,
			utf8_encode('Nombres') => $Nombres,
			utf8_encode('Tipo_Documento') => $TipoDocumento,
			utf8_encode('aceptacion_terminos') => $AceptacionTerminos,
			utf8_encode('Fecha_aceptacion_terminos') => $DateRegister,
			utf8_encode('url_source') => $url_source,
			utf8_encode('utm_Medium') => $utm_medium,
			utf8_encode('utm_campaign') => $utm_campaign,
			utf8_encode('utm_content') =>  $utm_content,
			utf8_encode('utm_source') =>  $utm_source,
			utf8_encode('utm_term') => $utm_term,
            utf8_encode('fbclid') => $fbclid,
            utf8_encode('AceptaPublicidad') => $AceptaPublicidad,
            utf8_encode('leadId') => $leadId,


        );
            //utf8_encode('AceptaPublicidad') => $AceptaPublicidad //Actualizar segun se defina en el crm 

        //$wsdl = 'http://online.toulouse.edu/wslanding/Service1.svc?wsdl';
        $wsdl = 'https://wsl.tls.edu.pe/wsl/Service1.svc?singleWsdl';
		$client = new SoapClient($wsdl, array("trace"=>1, "exceptions"=>0));
		$value = $client->Insertar(array('persona' => $params));
		$respuesta = $client->__getLastResponse();
		$seEnvia = $client->__getLastRequest();
		$respuesta =  explode('><', $respuesta );
        print json_encode($respuesta);
        
	} catch (SoapFault $fault) {
		$responseWSCRM = $fault->faultstring;
        print json_encode($responseWSCRM);

	}


});


$app->post('/api/post-leads-new-crm', function(Request $request, Response $response ){
    $AnioFinColegio = $request->getParam('AnioFinColegio');
    $ApellidoPaterno = $request->getParam('ApellidoPaterno');
    $ApellidoMaterno = $request->getParam('ApellidoMaterno');
    $Carrera = $request->getParam('Carrera');
    $Celular = $request->getParam('Celular');
    $Documento = $request->getParam('Documento');
    $Email = $request->getParam('Email');
    $FormularioURL = $request->getParam('FormularioURL');
    $GradoAcademico = $request->getParam('GradoAcademico');
    $Indicador = $request->getParam('Indicador');
    $Indicador_2 = $request->getParam('Indicador_2');
    $MedioContacto = $request->getParam('MedioContacto');
    $Mensaje = $request->getParam('Mensaje');
    $Mundo = $request->getParam('Mundo');
    $Nombres = $request->getParam('Nombres');
    $Sede = $request->getParam('Sede');
    $TipoContacto = $request->getParam('TipoContacto');
    $TipoDocumento = $request->getParam('TipoDocumento');
    $AceptacionTerminos = $request->getParam('AceptacionTerminos');
    $FechaAceptacionTerminos = $request->getParam('FechaAceptacionTerminos');
    $url_source = $request->getParam('url_source');
    $utm_medium = $request->getParam('utm_medium');
    $utm_campaign = $request->getParam('utm_campaign');
    $utm_content = $request->getParam('utm_content');
    $utm_source = $request->getParam('utm_source');
    $utm_term = $request->getParam('utm_term');
    $fbclid = $request->getParam('fbclid');
    $DateRegister = date("m/d/Y h:i");
    $AceptaPublicidad = $request->getParam('AceptaPublicidad');
    $cookieGA = $request->getParam('cookieGA');
    $leadId =  hash('adler32',$request->getParam('Email'));
    
    //print json_encode($params);
    function changenull($value){
        if(isset($value)){
            return $value;
        }else{
            return '';
        }
    }

    $params = '{
        "nombre": "'.changenull($Nombres).'",
        "apellidoPaterno": "'.changenull($ApellidoPaterno).'",
        "apellidoMaterno": "'.changenull($ApellidoMaterno).'",
        "telefono": "'.changenull($Celular).'",
        "email":  "'.changenull($Email).'",
        "carrera": "'.changenull($Carrera).'",
        "dni": "'.changenull($Documento).'",
        "fechaAceptacionTermino": "'.$DateRegister.'",
        "aceptacionTermino": "'.changenull($AceptacionTerminos).'",
        "gradoAcademico": "'.$Indicador.'",
        "UNE": "TLS",
        "utmSource":  "'.changenull($utm_source).'",
        "utmMedium": "'.changenull($utm_medium).'",
        "utmCampaign": "'.changenull($utm_campaign).'",
        "utmTerm": "'.changenull($utm_term).'",
        "utmContent":  "'.changenull($utm_content).'",
        "formularioOrigen": "'.changenull($Indicador_2).'",
        "genero":"",
        "direccion":"",
        "departamento":"",
        "provincia":"",
        "distrito":"",
        "gradoEstudio":"",
        "anioFin": "'.changenull($AnioFinColegio).'",
        "colegio":"",
        "direccionColegio":"",
        "departamentoColegio":"",
        "provinciaColegio":"",
        "distritoColegio":"",
        "carrera_2":"",
        "carrera_3":"",
        "aceptaPublicidad": "'.$AceptaPublicidad.'",
        "fechaAceptaPublicidad": "'.$DateRegister.'",
        "idClienteWeb": "'. changenull($cookieGA).'",
        "idLead": "'.changenull($leadId).'",
        "telefonoFijo":"",
        "comentario":"'.changenull($Mensaje).'",
        "tipoDocumento": "DNI",
        "fechaNacimiento":"",
        "urlSource": "'.changenull($url_source).'",
        "fbclid": "'.changenull($fbclid).'",
        "gclid": ""
    }';

        function getToken(){
            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://wscomercial.ieduca.pe/seguridad/oauth2/token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'client_id=gchpi9tbxzakguvnes8kijc3fx7mbznf.app.com&client_secret=ztdkdkmfedwcz6i8ddwnmz3n2x10a33k',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
            ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            $response = json_decode($response, true);
            return $response['access_token'];
        }
        $token = getToken();
        $abt = "Authorization: Bearer $token";
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://wscomercial.ieduca.pe/comercial-prc/leadWeb',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => $params,

          CURLOPT_HTTPHEADER => array(
            $abt,
            'Content-Type: application/json'
          ),
        ));
           $server_output = curl_exec($curl);    
           print json_encode (array ($params,$server_output));
        curl_close($curl);
    
});