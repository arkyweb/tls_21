let paso_1 = document.querySelector('#step_1'),
    paso_2 = document.querySelector('#step_2'),
    fieldsPaso_1 = document.querySelectorAll('#tls_form .step_1 input'),
    fieldsPaso_2 = document.querySelectorAll('#tls_form .step_2 input, #tls_form .step_2 select '),
    tls_form = document.querySelector("#tls_form");

let recorreValidar = (fields ) => {
    for (let j = 0; j < fields.length; j++) {
        //Ejecución por cada field
        fields[j].addEventListener('input', function ({ target }) {
            validar(target);
        })    
    }
}
//Llamando a la validacion de campo en campo
recorreValidar(fieldsPaso_1);

//Validar para el siguiente paso
document.querySelector('#siguiente').addEventListener('click',function(e){
  e.preventDefault();
  let jocker = [];
  for (let j = 0; j < fieldsPaso_1.length; j++) {
        //Validacion total 
        validar(fieldsPaso_1[j]);
        if(fieldsPaso_1[j].parentNode.classList.contains('warning')){
          jocker.push(fieldsPaso_1[j])
          console.log(fieldsPaso_1[j])
        }
    }

    if(jocker.length === 0){
      paso_1.classList.add('ocultar')
      paso_2.classList.remove('ocultar')
      }
})
//anterior
document.querySelector('#anterior').addEventListener('click',function(e){
    e.preventDefault();
    paso_1.classList.remove('ocultar');
    paso_2.classList.add('ocultar');
})

/** Combos */
let tipoProducto = document.querySelector("#tipoProducto"),
producto = document.querySelector("#producto"),
sedesCampus = document.querySelector("#sedesCampus");
(tipoProducto.length = 0), (producto.length = 0), (sedesCampus.length = 0);
let defaultOptionT = document.createElement("option"),
    defaultOptionP = document.createElement("option"),
    defaultOptionSC = document.createElement("option");
    (defaultOptionT.text = "- Seleccionar -"),
    (defaultOptionP.text = "- Seleccionar -"),
    (defaultOptionSC.text = "- Seleccionar -"),
    (defaultOptionT.value = " "),
    (defaultOptionP.value = " "),
    (defaultOptionSC.value = " "),
    tipoProducto.add(defaultOptionT),
    producto.add(defaultOptionP),
    sedesCampus.add(defaultOptionSC);

    var parametros = document.querySelectorAll("#core_tipo, #core_id");
    console.log(parametros.length);
    var url = "/apiProductos";
    1 < parametros.length &&
        (url = `/apiProductosContextual/${parametros[1].textContent}/${parametros[0].textContent}`);
    var myArray = [];
    
    fetch(url)
        .then(function (a) {
            if (a.ok) return a.json();
        })
        .then(function (a) {
            (result = a.reduce(function (a, b) {
                return (a[b.tipo] = (a[b.tipo] || []).concat(b)), a;
            }, {})),
                localStorage.setItem("resultados", JSON.stringify(result));
        })
        .then(function () {
            var a = Object.keys(result);
            a = a.sort();
            for (var b = 0; b < a.length; b++)
                if (
                    ((option = document.createElement("option")),
                        (option.text = a[b]),
                        (option.value = a[b]),
                        tipoProducto.appendChild(option),
                        1 < parametros.length)
                ) {
                    (defaultOptionT.text = a[b]),
                        (defaultOptionT.value = a[b]),
                        (defaultOptionP.text = result[a[b]][b].carrera),
                        (defaultOptionP.value = result[a[b]][b].carrera);
                    for (var c = result[a[b]][b].sedes, d = c.split(/\s*,\s*/).sort(), e = 0; e < d.length; e++)
                        (option = document.createElement("option")),
                            (option.text = d[e]),
                            (option.value = d[e]),
                            sedesCampus.appendChild(option);
                    (tipoProducto.disabled = !0), (producto.disabled = !0), (sedesCampus.disabled = !1);
                }
        }),
        tipoProducto.addEventListener("change", function () {
            if (1 != this.value.length) {
                var a = JSON.parse(localStorage.getItem("resultados"));
                producto.innerHTML = "";
                var b = document.createElement("option");
                (producto.length = 0),
                    (b.text = "Cargando...."),
                    (b.value = " "),
                    producto.add(b),
                    (a[this.value] = a[this.value].sort(compare));
                for (var c = 0; c < a[this.value].length; c++)
                    (option = document.createElement("option")),
                        (option.text = a[this.value][c].carrera),
                        (option.value = a[this.value][c].carrera),
                        producto.appendChild(option);
                (a = []),
                    (b.text = "- Seleccionar -"),
                    (producto.disabled = !1),
                    (sedesCampus.disabled = !0),
                    (sedesCampus.innerHTML = ""),
                    (sedesCampus.length = 0);
                var d = document.createElement("option");
                (d.text = "- Seleccionar -"), (d.value = " "), sedesCampus.add(d);
            }
        }),
        producto.addEventListener("change", function () {
            if (1 != this.value.length) {
                (sedesCampus.innerHTML = ""), (sedesCampus.length = 0);
                var a = document.createElement("option");
                (a.text = "- Seleccionar -"), (a.value = " "), sedesCampus.add(a);
                for (
                    var b = JSON.parse(localStorage.getItem("resultados")),
                    c = b[tipoProducto.value].find((a) => a.carrera === this.value),
                    d = c.sedes,
                    e = d.split(/\s*,\s*/).sort(),
                    f = 0;
                    f < e.length;
                    f++
                )
                    (option = document.createElement("option")),
                        (option.text = e[f]),
                        (option.value = e[f]),
                        sedesCampus.appendChild(option);
                (a.text = "- Seleccionar -"), (sedesCampus.disabled = !1);
            }
        });
    
    function compare(c, a) {
        const b = c.carrera,
            d = a.carrera;
        let e = 0;
        return b > d ? (e = 1) : b < d && (e = -1), e;
    }
    
    tls_form.addEventListener("submit", function (a) {
      a.preventDefault();
      // validateForn("#tls_form input, #tls_form select, #tls_form #terCondiciones");
      // var b = validateForn("#tls_form input, #tls_form select, #tls_form #terCondiciones");

      let comodin = []
      for (let j = 0; j < fieldsPaso_2.length; j++) {
            //Validacion total 
            validar(fieldsPaso_2[j])
            if(fieldsPaso_2[j].parentNode.classList.contains('warning')){
                comodin.push(fieldsPaso_2[j])
            }
         
        }



      if (0 === comodin.length) {
          function a(a) {
              a = a.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
              var b = new RegExp("[\\?&]" + a + "=([^&#]*)"),
                  c = b.exec(location.search);
              return null === c ? "" : decodeURIComponent(c[1].replace(/\+/g, " "));
          }
          var c,
              d,
              e = a("utm_source"),
              f = a("utm_medium"),
              g = a("utm_campaign"),
              h = a("utm_content"),
              i = a("utm_term"),
              j = a("fbclid");
          switch (tipoProducto.value) {
              case "Carreras Profesionales":
                  (c = "PRGT"), (d = "AG");
                  break;
              case "Carreras Profesionales T\xC3\xA9cnicas":
                  (c = "PRGT"), (d = "AG");
                  break;
              case "Cursos":
                  (c = "EXTT"), (d = "EC");
                  break;
              case "Diplomados":
                  (c = "EXTT"), (d = "EC");
          }
          if ("on" == terCondiciones.value) var k = "SI";
          let b;
          var l = document.querySelector("#terPromos").checked;
          !0 === l ? (b = "SI") : !1 === l ? (b = "NO") : void 0;
          var m = {
              Nombres: document.querySelector('#nombres').value,
              ApellidoPaterno: document.querySelector('#paterno').value,
              ApellidoMaterno: document.querySelector('#materno').value,
              Email: document.querySelector('#correo').value,
              Celular: document.querySelector('#celular').value,
              Documento: document.querySelector('#dni').value,
              Mundo: tipoProducto.value,
              Carrera: producto.value,
              Sede: sedesCampus.value,
              AceptacionTerminos: k,
              utm_source: e,
              utm_medium: f,
              utm_campaign: g,
              utm_content: h,
              utm_term: i,
              fbclid: j,
              AnioFinColegio: document.querySelector("#egreso").value,
              Indicador: c,
              Indicador_2: d,
              url_source: window.location.protocol + "//" + window.location.hostname + window.location.pathname,
              AceptaPublicidad: b,
              cookieGA: geTCookie("_ga").split(".")[2] + "." + geTCookie("_ga").split(".")[3],
          };

          console.log(m);
          fetch("/api/post-leads-new-crm", {
              method: "POST",
              body: JSON.stringify(m),
              headers: {
                  "Content-Type": "application/json",
              },
          })
              .then((a) => a.json())
              .then((a) => {
                  console.log(a);
              })
              .then((document.querySelector("#enviando").disabled = !0))
              .then((window.location.href = "/gracias"))
              .catch(function (a) {
                  console.log("Hubo un problema con la petici\xC3\xB3n Fetch:" + a.message);
              }),
              fetch("/api/post-leads", {
                  method: "POST",
                  body: JSON.stringify(m),
                  headers: {
                      "Content-Type": "application/json",
                  },
              })
                  .then((a) => a.json())
                  .then((a) => {
                      console.log(a);
                  })
                  .then((document.querySelector("#enviando").disabled = !0))
                  .catch(function (a) {
                      console.log("Hubo un problema con la petici\xC3\xB3n Fetch:" + a.message);
                  });
      }
  });