
        var apiServicio = '/api/get-leads';
        fetch(apiServicio)
                    .then(function (response) {

                        if (response.ok) {
                            return response.json();
                        }
                    })
                    .then(function (data) {
                        table_headTable();
                       var tempArray = [];
                       tamanoPorcion = 25;
                       for(var i = 0; i < data.length; i+=tamanoPorcion){
                             myPorcion = data.slice(i, i+tamanoPorcion);
                             tempArray.push(myPorcion);
                        }


                       console.log(tempArray)
                       console.log(tempArray.length)                 
                       for (let i = 0; i < tempArray[0].length; i++) {
                                creaCeldas(tempArray[0][i]);

                            }
                            var ul = document.createElement('ul');
                            ul.classList.add('uk-pagination');
                        for (let k = 0; k < tempArray.length; k++) {
                            
                            var li = document.createElement('li');
                            li.id = k ;
                            li.textContent = k
                            ul.appendChild(li);
                            document.querySelector('.paginador').appendChild(ul)
                        }


                        var listArr = document.querySelectorAll('tfoot.paginador ul li');
                        for (let m = 0; m < listArr.length; m++) {
                            listArr[m] .addEventListener('click', function(e){
                                console.log(this.id)
                                document.querySelector('.registros_lead').innerHTML='';
                                for (let i = 0; i < tempArray[this.id].length; i++) {
                                    
            creaCeldas(tempArray[this.id][i]);
    
                                }
                            })
                            
                        }
                       

                    });


function creaCeldas(objeto){
    var tr = document.createElement('tr');
    for (let j in objeto) {
        var cell = document.createElement("td");
        cell.textContent = objeto[j];
        tr.appendChild(cell);
        document.querySelector('.registros_lead').appendChild(tr)
    }
}

function table_headTable(){
    var tabla = document.createElement('table');
    //tabla.style.width = '100%';
    //tabla.setAttribute('border', '1');
    tabla.classList.add('uk-table', 'uk-table-justify', 'uk-table-striped', 'uk-table-responsive')
    var thead = document.createElement('thead');
    var tr = document.createElement('tr');
    document.querySelector('.tabla-container').appendChild(tabla);
    tabla.appendChild(thead);
    thead.appendChild(tr)

    //tbody table
    var tbody = document.createElement('tbody');
    tbody.classList.add('registros_lead');
    tabla.appendChild(tbody);
   // 
   var tfoot = document.createElement('tfoot');
   tfoot.classList.add('paginador');
   tabla.appendChild(tfoot);
    var headers = {
        ID:'ID', 
        AnioFinColegio:'Año de egreso', 
        ApellidoPaterno:'Apellido Paterno', 
        ApellidoMaterno:'Apellido Materno', 
        Nombres:'Nombres', 
        Documento: 'DNI', 
        Email: 'Correo', 
        Celular: 'Celular', 
        url_source: 'Formulario URL', 
        Indicador: 'Indicador', 
        Mundo: 'Mundo', 
        Carrera:'Carrera', 
        Sede:'Sede', 
        AceptacionTerminos:'Aceptación',
        DateRegister:'Fecha de Registro'
        
    };



    for (let j in headers) {
        var cell = document.createElement("th");
        cell.classList.add('color-rosa')
        cell.textContent = headers[j];
        tr.appendChild(cell);
    }
}



/**
 * Download Leads to csc per ranfe date
 */
var inicio = document.querySelector('#inicio')
var final = document.querySelector('#final')
 document.querySelector('#leads_fechas').addEventListener('submit', function(e){
     e.preventDefault()
     console.log()
    var urlDate = `api/get-leads/${inicio.value}/${final.value}`
     fetch(urlDate)
    .then(function (response) {
        if (response.ok) {
            return response.json();
        }
        
    })
    .then(function (data) {
        console.log(data)
        exportCSVFile(headers, data , 'Leads TLS');

    })
 })

 function convertToCSV(objArray) {
    //var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    var array = objArray
    var str = '';

    for (var i = 0; i < array.length; i++) {
        var line = '';
        for (var index in array[i]) {
            if (line != '') line += ','

            line += array[i][index];
        }

        str += line + '\r\n';
    }

    return str;
}

function exportCSVFile(headers, items, fileTitle) {
    if (headers) {
        items.unshift(headers);
    }

    // Convert Object to JSON
    //var jsonObject = JSON.stringify(items);
    var BOM = "\uFEFF";
    var csv = this.convertToCSV(items);
    var csv2 = BOM + csv;
    var exportedFilenmae = fileTitle + '.csv' || 'export.csv';
    

    var blob = new Blob([csv2], {type: "text/csv;charset=utf-8" });
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(blob, exportedFilenmae);
    } else {
        var link = document.createElement("a");
        if (link.download !== undefined) { // feature detection
            // Browsers that support HTML5 download attribute
            var url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", exportedFilenmae);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
}


var headers = {
    ID:'ID', 
    AnioFinColegio:'Año de egreso', 
    ApellidoPaterno:'Apellido Paterno', 
    ApellidoMaterno:'Apellido Materno', 
    Nombres:'Nombres', 
    Documento: 'DNI', 
    Email: 'Correo', 
    Celular: 'Celular', 
    url_source: 'Formulario URL', 
    Indicador: 'Indicador', 
    Mundo: 'Mundo', 
    Carrera:'Carrera', 
    Sede:'Sede', 
    AceptacionTerminos:'Aceptación',
    DateRegister:'Fecha de Registro'
};

