var nombres = document.querySelector('#nombres')
    var paterno = document.querySelector('#paterno')
    var materno = document.querySelector('#materno')
    var correo = document.querySelector('#correo')
    var celular = document.querySelector('#celular')
    var dni = document.querySelector('#dni')
    var tipoProducto = document.querySelector('#tipoProducto')
    var producto = document.querySelector('#producto')
    var sedesCampus = document.querySelector('#sedesCampus')
    var terCondiciones = document.querySelector('#terCondiciones')
    var tls_form = document.querySelector('#tls_form')
    var regex = /[a-z]/gi // busca cacteres del ABCD
    var regexDos = /\W/g // busca caracteres especiales
    var regexMail = /\b[\w]+@[\w-]+(?:\.[\w]+)+\b/ // busca caracteres especiales

    tipoProducto.length = 0
    producto.length = 0
    sedesCampus.length = 0
    var defaultOptionT = document.createElement('option')
    var defaultOptionP = document.createElement('option')
    var defaultOptionSC = document.createElement('option')
    defaultOptionT.text = "- Seleccionar -"
    defaultOptionP.text = "- Seleccionar -"
    defaultOptionSC.text = "- Seleccionar -"
    defaultOptionT.value = " "
    defaultOptionP.value = " "
    defaultOptionSC.value = " "

    tipoProducto.add(defaultOptionT)
    producto.add(defaultOptionP)
    sedesCampus.add(defaultOptionSC)

    var parametros = document.querySelectorAll('#core_tipo, #core_id')
    console.log (parametros.length)
    var url = '/apiProductos'
    
    if( parametros.length > 1 ){
        url = `/apiProductosContextual/${parametros[1].textContent}/${parametros[0].textContent}`
    }

    var myArray = []
    fetch(url)
        .then(function (response) {
            if (response.ok) {
                return response.json();
            }
        })
        .then(function (data) {
            //console.log(data)
            result = data.reduce(function (h, obj) {
                h[obj.tipo] = (h[obj.tipo] || []).concat(obj);
                return h;
            }, {})

            localStorage.setItem("resultados", JSON.stringify(result));
        })
        .then(function (data) {
            var nameMundos = Object.keys(result)
            nameMundos = nameMundos.sort()
            for (var j = 0; j < nameMundos.length; j++) {
                option = document.createElement('option');
                option.text = nameMundos[j];
                option.value = nameMundos[j];
                tipoProducto.appendChild(option);

                if( parametros.length > 1 ){
                    defaultOptionT.text = nameMundos[j]
                    defaultOptionT.value = nameMundos[j]
                    defaultOptionP.text = result[nameMundos[j]][j].carrera
                    defaultOptionP.value = result[nameMundos[j]][j].carrera
                    //console.log(result[nameMundos[j]][j])

                    var nombres = result[nameMundos[j]][j].sedes ;
                    var expresionRegular = /\s*,\s*/;
                    var listaNombres = nombres.split(expresionRegular).sort();
                    //console.log(listaNombres)
                    for (var i = 0; i < listaNombres.length; i++) {
                        option = document.createElement('option');
                        option.text = listaNombres[i];
                        option.value = listaNombres[i];
                        sedesCampus.appendChild(option);
        
                    }
                    tipoProducto.disabled = true
                    producto.disabled = true
                    sedesCampus.disabled = false
                }


            }
            
            //defaultOptionT.text = "- Seleccionar -"

        })


    tipoProducto.addEventListener('change', function (e) {
        if(this.value.length != 1){
            var result = JSON.parse(localStorage.getItem('resultados'))
            ///console.log( result )
            producto.innerHTML = ''
            var defaultOptionP = document.createElement('option')
            producto.length = 0
            defaultOptionP.text = "Cargando...."
            defaultOptionP.value = " "
            producto.add(defaultOptionP)


            result[this.value] =  result[this.value].sort(compare)
            //console.log( result[this.value].sort(compare) )
            for (var j = 0; j < result[this.value].length; j++) {
                option = document.createElement('option');
                option.text = result[this.value][j].carrera;
                option.value = result[this.value][j].carrera;
                producto.appendChild(option);
    
            }
            result = []
            defaultOptionP.text = "- Seleccionar -"
            producto.disabled = false
            sedesCampus.disabled = true
            sedesCampus.innerHTML = ''
            sedesCampus.length = 0
            var defaultOptionSC = document.createElement('option')
            defaultOptionSC.text = "- Seleccionar -"
            defaultOptionSC.value = " "
            sedesCampus.add(defaultOptionSC)
        }

    })


    producto.addEventListener('change', function (e) {
        if(this.value.length != 1){
            sedesCampus.innerHTML = ''
            sedesCampus.length = 0
            var defaultOptionSC = document.createElement('option')
            defaultOptionSC.text = "- Seleccionar -"
            defaultOptionSC.value = " "
            sedesCampus.add(defaultOptionSC)
            var result = JSON.parse(localStorage.getItem('resultados'))
            //console.log(result[tipoProducto.value])
            var resultado = result[tipoProducto.value].find(fruta => fruta.carrera === this.value);
            //console.log(resultado.sedes)
            var nombres = resultado.sedes
            var expresionRegular = /\s*,\s*/;
            var listaNombres = nombres.split(expresionRegular).sort();
            //console.log(listaNombres)
            for (var i = 0; i < listaNombres.length; i++) {
                option = document.createElement('option');
                option.text = listaNombres[i];
                option.value = listaNombres[i];
                sedesCampus.appendChild(option);

            }
            defaultOptionSC.text = "- Seleccionar -"
            sedesCampus.disabled = false
      }
    });

function compare(a, b) {
    const genreA = a.carrera;
    const genreB = b.carrera;

    let comparison = 0;
    if (genreA > genreB) {
        comparison = 1;
    } else if (genreA < genreB) {
        comparison = -1;
    }
    return comparison;
}



/** Form Validate Function */
    function validateForn( campos ){
        var inputs = document.querySelectorAll(campos)
        var arrTest = []
        for (var  i = 0; i < inputs.length ; i++) {
            if(inputs[i].value.length <= 1 && inputs[i].getAttribute('name') != 'correo'){
                inputs[i].classList.add('uk-form-danger');
                inputs[i].classList.remove('uk-form-success')
                inputs[i].nextSibling.nextSibling.classList.remove('uk-display-none')
                inputs[i].nextSibling.nextSibling.classList.add('uk-form-danger')
                arrTest.push(i)
            }else{
                inputs[i].classList.add('uk-form-success');
                inputs[i].nextSibling.nextSibling.classList.add('uk-display-none')
                inputs[i].nextSibling.nextSibling.classList.remove('uk-form-danger')
            }
            if(inputs[i].type == 'tel'){
                if(inputs[i].value.length != inputs[i].getAttribute("maxlength") ){
                    inputs[i].classList.add('uk-form-danger');
                    inputs[i].classList.remove('uk-form-success');
                    inputs[i].nextSibling.nextSibling.classList.remove('uk-display-none')
                    inputs[i].nextSibling.nextSibling.classList.add('uk-form-danger')
                    arrTest.push(i)
                }
            }
            //if(inputs[i].type == 'checkbox'){
            if(inputs[i].getAttribute('name') == 'terCondiciones'){

                if(inputs[i].checked ){
                    inputs[i].classList.remove('uk-form-danger');
                    document.querySelector('.terCondiciones').classList.add('uk-display-none')
                }else{
                    inputs[i].classList.add('uk-form-danger');
                    inputs[i].classList.remove('uk-form-success');
                    document.querySelector('.terCondiciones').classList.remove('uk-display-none')
                    document.querySelector('.terCondiciones').classList.add('uk-form-danger')
                    arrTest.push(i)
                }
            }
            if(inputs[i].getAttribute('name') == 'correo'){
                //console.log(inputs[i].value.match(regexMail))
                if(  inputs[i].value.match(regexMail) == null ){
                        inputs[i].classList.add('uk-form-danger');
                        inputs[i].classList.remove('uk-form-success');
                        inputs[i].nextSibling.nextSibling.classList.remove('uk-display-none')
                        inputs[i].nextSibling.nextSibling.classList.add('uk-form-danger')
                        arrTest.push(i)
                    }else{
                        inputs[i].classList.add('uk-form-success')
                        inputs[i].nextSibling.nextSibling.classList.add('uk-display-none')
                        inputs[i].nextSibling.nextSibling.classList.remove('uk-form-danger')
                    }
            }
        }
    
        return arrTest.length
    }
    var solonumeros = document.querySelectorAll("input[type='tel']")
    for (var i = 0; i < solonumeros.length; i++) {
        solonumeros[i].addEventListener('input',function(e){
            if( this.value.match(regex) ){
                    var onlyNumbers = this.value.replace(regex, '');
                    this.value = onlyNumbers
            }
            if( this.value.match(regexDos) ){
                    var onlyNumbersDos = this.value.replace(regexDos, '');
                    this.value = onlyNumbersDos
            }
            if(this.value.length != this.getAttribute("maxlength") ){
                this.classList.add('uk-form-danger');
                this.classList.remove('uk-form-success');
                this.nextSibling.nextSibling.classList.remove('uk-display-none')
                this.nextSibling.nextSibling.classList.add('uk-form-danger')
            }else{
                this.classList.add('uk-form-success')
                this.nextSibling.nextSibling.classList.add('uk-display-none')
                this.nextSibling.nextSibling.classList.remove('uk-form-danger')
            }
        })
        
    }
    document.querySelector('#correo').addEventListener('input', function(e){
        if(  this.value.match(regexMail) == null ){
                    this.nextSibling.nextSibling.classList.remove('uk-display-none')
                     this.nextSibling.nextSibling.classList.add('uk-form-danger')
                     this.classList.add('uk-form-danger');
                     this.classList.remove('uk-form-success');
                 }else{
                     this.classList.add('uk-form-success')
                     this.nextSibling.nextSibling.classList.add('uk-display-none')
                     this.nextSibling.nextSibling.classList.remove('uk-form-danger')
                 }
    })
/**End validate functions */

/** Validaciones paso-1 */
var campos_paso_1 = '#tls_form #nombres, #tls_form #paterno, #tls_form #materno, #tls_form #correo ,#tls_form #celular, #tls_form #dni'
document.querySelector('#siguiente').addEventListener('click',function(e){
    e.preventDefault()
    validateForn( campos_paso_1 )
    var validandoPaso_1 = validateForn(campos_paso_1)
    console.log(validandoPaso_1)
        if(validandoPaso_1 === 0 ){
            document.querySelector('.paso-1').classList.add('uk-hidden')
            document.querySelector('.paso-2').classList.remove('uk-hidden')
            document.querySelector('.barra-paso-1').classList.add('barra-listo')
            document.querySelector('.barra-paso-1').classList.remove('barra-activo')
            document.querySelector('.barra-paso-2').classList.add('barra-activo')
        }

    })
    
    document.querySelector('#anterior').addEventListener('click',function(e){
        e.preventDefault()
        document.querySelector('.paso-1').classList.remove('uk-hidden')
        document.querySelector('.paso-2').classList.add('uk-hidden')
        document.querySelector('.barra-paso-1').classList.remove('barra-listo')
        document.querySelector('.barra-paso-1').classList.add('barra-activo')
        document.querySelector('.barra-paso-2').classList.remove('barra-activo')
    })


/**End validate functions */

/**Submit form */
    tls_form.addEventListener('submit', function (e) {
        e.preventDefault()
        var caposValidar = '#tls_form input, #tls_form select, #tls_form #terCondiciones'
        validateForn(caposValidar)
        var validando = validateForn(caposValidar)
        if (validando === 0){
            //Function get UTM
            function getParameterByName(name) {
                name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                    results = regex.exec(location.search);
                return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
            }
    
            var utm_source = getParameterByName('utm_source')
            var utm_medium = getParameterByName('utm_medium')
            var utm_campaign = getParameterByName('utm_campaign')
            var utm_content = getParameterByName('utm_content')
            var utm_term = getParameterByName('utm_term')
            var fbclid = getParameterByName('fbclid')
            var indicador, indicador_2
            switch (tipoProducto.value) {
                case 'Carreras Profesionales':
                        indicador = 'PRGT'
                        indicador_2 = 'AG'
                    break;
                case 'Carreras Profesionales Técnicas':
                    indicador = 'PRGT'
                    indicador_2 = 'AG'
                    break;
                case 'Cursos':
                    indicador = 'EXTT'
                    indicador_2 = 'EC'
                    break;
                case 'Diplomados':
                    indicador = 'EXTT'
                    indicador_2 = 'EC'
                    break;
            }

            if (terCondiciones.value == 'on'){
                var acepto = 'SI'
            }
            let AceptaPublicidad;
            var terPromos = document.querySelector('#terPromos').checked
            switch (terPromos) {
                case true:
                    AceptaPublicidad = 'SI'
                    break;
                case false:
                    AceptaPublicidad = 'NO'
                    break;
            }


            var formData = {
                "Nombres": nombres.value,
                "ApellidoPaterno": paterno.value,
                "ApellidoMaterno": materno.value,
                "Email": correo.value,
                "Celular": celular.value,
                "Documento": dni.value,
                "Mundo": tipoProducto.value,
                "Carrera": producto.value,
                "Sede": sedesCampus.value,
                "AceptacionTerminos": acepto,
                "utm_source": utm_source,
                "utm_medium": utm_medium,
                "utm_campaign": utm_campaign,
                "utm_content": utm_content,
                "utm_term": utm_term,
                "fbclid": fbclid,
                'AnioFinColegio': document.querySelector('#egreso').value,
                'Indicador': indicador,
                'Indicador_2':indicador_2,
                //'url_source': window.location.href,
                //'url_source': tls_form.getAttribute('id'),
                'url_source': window.location.protocol+'//'+window.location.hostname + window.location.pathname,
                'AceptaPublicidad':AceptaPublicidad,
                'cookieGA': geTCookie('_ga').split('.')[2]+'.'+geTCookie('_ga').split('.')[3]
            }
            //console.log(formData)
    
            var service = '/api/post-leads-new-crm' // For nginx
            var service_persistent = '/api/post-leads' // For nginx
    
            //var service ='/apiRest/public/api/post-leads' //For Apache
            fetch(service, {
                method: 'POST',
                body: JSON.stringify(formData),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json())
                .then(data => {
                    console.log(data);
                })
                .then(document.querySelector('#enviando').disabled = true)
                .then(window.location.href = "/gracias")
                .catch(function (error) {
                    console.log('Hubo un problema con la petición Fetch:' + error.message);
                })
    
            fetch(service_persistent, {
                method: 'POST',
                body: JSON.stringify(formData),
                headers: {
                    'Content-Type': 'application/json'
                }
                }).then(res => res.json())
                    .then(data => {
                        console.log(data);
                    })
                    .then(document.querySelector('#enviando').disabled = true)
                    
                    .catch(function (error) {
                        console.log('Hubo un problema con la petición Fetch:' + error.message);
                    })
    
        }

    })