var nombresEvent = document.querySelector('#event_nombres')
var paternoEvent = document.querySelector('#event_paterno')
var maternoEvent = document.querySelector('#event_materno')
var correoEvent = document.querySelector('#event_correo')
var celularEvent = document.querySelector('#event_celular')
var dniEvent = document.querySelector('#event_dni')
var terCondicionesEvent = document.querySelector('#event_terCondiciones')
var tls_formEvent = document.querySelector('#event_tls_form')
var regex = /[a-z]/gi // busca cacteres del ABCD
var regexDos = /\W/g // busca caracteres especiales
var regexMail = /\b[\w]+@[\w-]+(?:\.[\w]+)+\b/ // busca caracteres especiales






//cambia por el identificador del form, en el futuro sera un funcion
document.querySelector('#event_correo').addEventListener('input', function(e){
    if(  this.value.match(regexMail) == null ){
                 this.nextSibling.nextSibling.classList.remove('uk-display-none')
                 this.nextSibling.nextSibling.classList.add('uk-form-danger')
                 this.classList.add('uk-form-danger');
                 this.classList.remove('uk-form-success');
             }else{
                 this.classList.add('uk-form-success')
                 this.nextSibling.nextSibling.classList.add('uk-display-none')
                 this.nextSibling.nextSibling.classList.remove('uk-form-danger')
             }
})

tls_formEvent.addEventListener('submit', function (e) {
e.preventDefault()
var caposValidar = '#event_tls_form input, #event_tls_form #call_terCondiciones'
validateForn(caposValidar)
var validando = validateForn(caposValidar)
if (validando === 0){
    //Function get UTM
    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    var utm_source = getParameterByName('utm_source')
    var utm_medium = getParameterByName('utm_medium')
    var utm_campaign = getParameterByName('utm_campaign')
    var utm_content = getParameterByName('utm_content')
    var utm_term = getParameterByName('utm_term')
    var fbclid = getParameterByName('fbclid')

    if (terCondicionesEvent.value == 'on'){
        var acepto = 'SI'
    }

    let AceptaPublicidad;
    var event_terPromos = document.querySelector('#event_terPromos').checked
    switch (event_terPromos) {
        case true:
            AceptaPublicidad = 'SI'
            break;
        case false:
            AceptaPublicidad = 'NO'
            break;
    }

    var formData = {
        "Nombres": nombresEvent.value,
        "ApellidoPaterno": paternoEvent.value,
        "ApellidoMaterno": maternoEvent.value,
        "Email": correoEvent.value,
        "Celular": celularEvent.value,
        "Documento": dniEvent.value,
        "AceptacionTerminos": acepto,
        "utm_source": utm_source,
        "utm_medium": utm_medium,
        "utm_campaign": utm_campaign,
        "utm_content": utm_content,
        "utm_term": utm_term,
        "fbclid": fbclid,
        // 'Indicador': indicador,
        'url_source': tls_formEvent.getAttribute('id'),
        'AceptaPublicidad':AceptaPublicidad

    }
    //console.log(formData)

    var service = '/api/post-leads-crm' // For nginx
    var service_persistent = '/api/post-leads' // For nginx

    //var service ='/apiRest/public/api/post-leads' //For Apache
    fetch(service, {
        method: 'POST',
        body: JSON.stringify(formData),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => res.json())
        .then(data => {
            console.log(data);
        })
        //    .then(window.location.href = "/gracias")
        .then(document.querySelector('#event_enviando').disabled = true)
        .then(window.location.href = "/gracias")
        // .then(window.location.href = "/" + "gracias" + window.location.pathname)
        .catch(function (error) {
            console.log('Hubo un problema con la petición Fetch:' + error.message);
        })

    fetch(service_persistent, {
        method: 'POST',
        body: JSON.stringify(formData),
        headers: {
            'Content-Type': 'application/json'
        }
        }).then(res => res.json())
            .then(data => {
                console.log(data);
            })
            .then(document.querySelector('#event_enviando').disabled = true)
            
            .catch(function (error) {
                console.log('Hubo un problema con la petición Fetch:' + error.message);
            })

}

})