console.log('contacto')


var nombresContact = document.querySelector('#contact_nombres')
var paternoContact = document.querySelector('#contact_paterno')
var maternoContact = document.querySelector('#contact_materno')
var correoContact = document.querySelector('#contact_correo')
var celularContact = document.querySelector('#contact_celular')
var dniContact = document.querySelector('#contact_dni')
var contact_mss=  document.querySelector('#contact_mss')
var terCondicionesContact = document.querySelector('#contact_terCondiciones')
var tls_formContact = document.querySelector('#contact_tls_form')
var regex = /[a-z]/gi // busca cacteres del ABCD
var regexDos = /\W/g // busca caracteres especiales
var regexMail = /\b[\w]+@[\w-]+(?:\.[\w]+)+\b/ // busca caracteres especiales

correoContact.addEventListener('input', function(e){
    if(  this.value.match(regexMail) == null ){
                 this.nextSibling.nextSibling.classList.remove('uk-display-none')
                 this.nextSibling.nextSibling.classList.add('uk-form-danger')
                 this.classList.add('uk-form-danger');
                 this.classList.remove('uk-form-success');
             }else{
                 this.classList.add('uk-form-success')
                 this.nextSibling.nextSibling.classList.add('uk-display-none')
                 this.nextSibling.nextSibling.classList.remove('uk-form-danger')
             }
})



tls_formContact.addEventListener('submit', function (e) {
e.preventDefault()
var caposValidar = '#contact_tls_form input, #contact_tls_form #call_terCondiciones'
validateForn(caposValidar)
var validando = validateForn(caposValidar)
if (validando === 0){
    //Function get UTM
    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    var utm_source = getParameterByName('utm_source')
    var utm_medium = getParameterByName('utm_medium')
    var utm_campaign = getParameterByName('utm_campaign')
    var utm_content = getParameterByName('utm_content')
    var utm_term = getParameterByName('utm_term')
    var fbclid = getParameterByName('fbclid')

    if (terCondicionesContact.value == 'on'){
        var acepto = 'SI'
    }
    let AceptaPublicidad;
    var contact_terPromos = document.querySelector('#contact_terPromos').checked
    switch (contact_terPromos) {
        case true:
            AceptaPublicidad = 'SI'
            break;
        case false:
            AceptaPublicidad = 'NO'
            break;
    }
    
    var formData = {
        "Nombres": nombresContact.value,
        "ApellidoPaterno": paternoContact.value,
        "ApellidoMaterno": maternoContact.value,
        "Email": correoContact.value,
        "Celular": celularContact.value,
        "Documento": dniContact.value,
        "Mensaje" : contact_mss.value,
        "AceptacionTerminos": acepto,
        "utm_source": utm_source,
        "utm_medium": utm_medium,
        "utm_campaign": utm_campaign,
        "utm_content": utm_content,
        "utm_term": utm_term,
        "fbclid": fbclid,
        'url_source': window.location.protocol+'//'+window.location.hostname + window.location.pathname,
        'Indicador': 'PRGT',
        'Indicador_2': 'AG',
        'FormularioURL': window.location.href,
        'AceptaPublicidad':AceptaPublicidad,
        'cookieGA': geTCookie('_ga').split('.')[2]+'.'+geTCookie('_ga').split('.')[3]

    }
    //console.log(formData)

    var service = '/api/post-leads-new-crm' // For nginx
    var service_persistent = '/api/post-leads' // For nginx

    //var service ='/apiRest/public/api/post-leads' //For Apache
    fetch(service, {
        method: 'POST',
        body: JSON.stringify(formData),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => res.json())
        .then(data => {
            console.log(data);
        })
        //    .then(window.location.href = "/gracias")
        .then(document.querySelector('#contact_enviando').disabled = true)
        .then(window.location.href = "/gracias")
        // .then(window.location.href = "/" + "gracias" + window.location.pathname)
        .catch(function (error) {
            console.log('Hubo un problema con la petición Fetch:' + error.message);
        })

    fetch(service_persistent, {
        method: 'POST',
        body: JSON.stringify(formData),
        headers: {
            'Content-Type': 'application/json'
        }
        }).then(res => res.json())
            .then(data => {
                console.log(data);
            })
            .then(document.querySelector('#contact_enviando').disabled = true)
            
            .catch(function (error) {
                console.log('Hubo un problema con la petición Fetch:' + error.message);
            })

}

})