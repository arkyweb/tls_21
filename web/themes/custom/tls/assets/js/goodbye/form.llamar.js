var nombresCall = document.querySelector('#call_nombres')
var paternoCall = document.querySelector('#call_paterno')
var maternoCall = document.querySelector('#call_materno')
var celularCall = document.querySelector('#call_celular')
var tls_formCall = document.querySelector('#call_tls_form')
var terCondicionesCall = document.querySelector('#call_terCondiciones')
//var regex = /[a-z]/gi // busca cacteres del ABCD
//var regexDos = /\W/g // busca caracteres especiales
//var regexMail = /\b[\w]+@[\w-]+(?:\.[\w]+)+\b/ // busca caracteres especiales




/**Submit form */
tls_formCall.addEventListener('submit', function (e) {
    e.preventDefault()
    var caposValidar = '#call_tls_form input, #call_tls_form select, #call_tls_form #call_terCondiciones'
    validateForn(caposValidar)
    var validando = validateForn(caposValidar)
    if (validando === 0){
        //Function get UTM
        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }

        var utm_source = getParameterByName('utm_source')
        var utm_medium = getParameterByName('utm_medium')
        var utm_campaign = getParameterByName('utm_campaign')
        var utm_content = getParameterByName('utm_content')
        var utm_term = getParameterByName('utm_term')
        var fbclid = getParameterByName('fbclid')
        // var indicador
        // switch (tipoProducto.value) {
        //     case 'Carreras Profesionales':
        //             indicador = 'AG'
        //         break;
        //     case 'Carreras Profesionales Técnicas':
        //         indicador = 'AG'
        //         break;
        //     case 'Cursos':
        //         indicador = 'EC'
        //         break;
        //     case 'Diplomados':
        //         indicador = 'EC'
        //         break;
        // }

        if (terCondicionesCall.value == 'on'){
            var acepto = 'SI'
        }
        var formData = {
            "Nombres": nombresCall.value,
            "ApellidoPaterno": paternoCall.value,
            "ApellidoMaterno": maternoCall.value,
            "Celular": celularCall.value,
            "AceptacionTerminos": acepto,
            "utm_source": utm_source,
            "utm_medium": utm_medium,
            "utm_campaign": utm_campaign,
            "utm_content": utm_content,
            "utm_term": utm_term,
            "fbclid": fbclid,
            // 'Indicador': indicador,
            'url_source': tls_formCall.getAttribute('id')
        }
        //console.log(formData)

        var service = '/api/post-leads-crm' // For nginx
        var service_persistent = '/api/post-leads' // For nginx

        //var service ='/apiRest/public/api/post-leads' //For Apache
        fetch(service, {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
            .then(data => {
                console.log(data);
            })
            //    .then(window.location.href = "/gracias")
            .then(document.querySelector('#call_enviando').disabled = true)
            .then(window.location.href = "/gracias")
            // .then(window.location.href = "/" + "gracias" + window.location.pathname)
            .catch(function (error) {
                console.log('Hubo un problema con la petición Fetch:' + error.message);
            })

        fetch(service_persistent, {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            }
            }).then(res => res.json())
                .then(data => {
                    console.log(data);
                })
                .then(document.querySelector('#call_enviando').disabled = true)
                
                .catch(function (error) {
                    console.log('Hubo un problema con la petición Fetch:' + error.message);
                })

    }

})