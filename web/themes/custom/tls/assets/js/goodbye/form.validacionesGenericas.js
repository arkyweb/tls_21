console.log('funciona el generico ')

/** Form Validate Function */
function validateForn( campos ){


    var inputs = document.querySelectorAll(campos)
    //inputs = inputs.pop();
    console.log(typeof (inputs) )

    var arrTest = []
    for (var  i = 0; i < inputs.length ; i++) {
        if(inputs[i].value.length <= 1 && inputs[i].getAttribute('name') != 'correo'){
            inputs[i].classList.add('uk-form-danger');
            inputs[i].classList.remove('uk-form-success')
            inputs[i].nextSibling.nextSibling.classList.remove('uk-display-none')
            inputs[i].nextSibling.nextSibling.classList.add('uk-form-danger')
            arrTest.push(i)
        }else{
            inputs[i].classList.add('uk-form-success');
            inputs[i].nextSibling.nextSibling.classList.add('uk-display-none')
            inputs[i].nextSibling.nextSibling.classList.remove('uk-form-danger')
        }
        if(inputs[i].type == 'tel'){
            if(inputs[i].value.length != inputs[i].getAttribute("maxlength") ){
                inputs[i].classList.add('uk-form-danger');
                inputs[i].classList.remove('uk-form-success');
                inputs[i].nextSibling.nextSibling.classList.remove('uk-display-none')
                inputs[i].nextSibling.nextSibling.classList.add('uk-form-danger')
                arrTest.push(i)
            }
        }
            //if(inputs[i].type == 'checkbox'){
        if(inputs[i].getAttribute('name') == 'terCondiciones'){
            if(inputs[i].checked ){
                inputs[i].classList.remove('uk-form-danger');
                inputs[i].parentNode.parentNode.childNodes[3].classList.add('uk-display-none')
            }else{
                inputs[i].classList.add('uk-form-danger');
                inputs[i].classList.remove('uk-form-success');
                inputs[i].parentNode.parentNode.childNodes[3].classList.remove('uk-display-none')
                inputs[i].parentNode.parentNode.childNodes[3].classList.add('uk-form-danger')
                arrTest.push(i)
            }
        }
        if(inputs[i].getAttribute('name') == 'correo'){
            //console.log(inputs[i].value.match(regexMail))
            if(  inputs[i].value.match(regexMail) == null ){
                    inputs[i].classList.add('uk-form-danger');
                    inputs[i].classList.remove('uk-form-success');
                    inputs[i].nextSibling.nextSibling.classList.remove('uk-display-none')
                    inputs[i].nextSibling.nextSibling.classList.add('uk-form-danger')
                    arrTest.push(i)
                }else{
                    inputs[i].classList.add('uk-form-success')
                    inputs[i].nextSibling.nextSibling.classList.add('uk-display-none')
                    inputs[i].nextSibling.nextSibling.classList.remove('uk-form-danger')
                }
        }



        
    }

    return arrTest.length
}
//Generico para todos los campos tipo tel
var solonumeros = document.querySelectorAll("input[type='tel']")
for (var i = 0; i < solonumeros.length; i++) {
    solonumeros[i].addEventListener('input',function(e){
        if( this.value.match(regex) ){
                var onlyNumbers = this.value.replace(regex, '');
                this.value = onlyNumbers
        }
        if( this.value.match(regexDos) ){
                var onlyNumbersDos = this.value.replace(regexDos, '');
                this.value = onlyNumbersDos
        }
        if(this.value.length != this.getAttribute("maxlength") ){
            this.classList.add('uk-form-danger');
            this.classList.remove('uk-form-success');
            this.nextSibling.nextSibling.classList.remove('uk-display-none')
            this.nextSibling.nextSibling.classList.add('uk-form-danger')
        }else{
            this.classList.add('uk-form-success')
            this.nextSibling.nextSibling.classList.add('uk-display-none')
            this.nextSibling.nextSibling.classList.remove('uk-form-danger')
        }
    })
    
}

/**End validate functions */

/** Get Cookie function*/

let geTCookie =  (name) => {
    let cookieArr = document.cookie.split(";");
    for(let i = 0; i < cookieArr.length; i++) {
        let cookiePair = cookieArr[i].split("=");
        /* Removing whitespace at the beginning of the cookie name
        and compare it with the given string */
        if(name == cookiePair[0].trim()) {
            // Decode the cookie value and return
            return decodeURIComponent(cookiePair[1]);
        }
    }
    // Return null if not found
    return null;
}

